#!/bin/bash
export DEBIAN_FRONTEND=noninteractive
echo "deb file:///repository ./" > /etc/apt/sources.list
apt-get -qq update
echo "installing apt-utils"
apt-get -qq install -y apt-utils --allow-unauthenticated > /dev/null
echo "installing mongodb-org"
apt-get -qq install -y mongodb-org --allow-unauthenticated > /dev/null
echo "installing nodejs"
apt-get -qq install -y nodejs --allow-unauthenticated > /dev/null
echo "installing redis-server"
apt-get -qq install -y redis-server --allow-unauthenticated > /dev/null
echo "installing git"
apt-get -qq install -y git --allow-unauthenticated > /dev/null
echo "Configurating"
export NODE_PATH=/repository/node_modules
