"use strict";
var http = require("http");


var port = 5000;


http.createServer(function (request, response) {

   // Send the HTTP header
   // HTTP Status: 200 : OK
   // Content Type: text/plain

   response.writeHead(200, {"Content-Type": "text/plain"});
   console.log(request);
   response.end("Hello World");
}).listen(port);

// Console will print the message
console.log("Server running on port "+port);
