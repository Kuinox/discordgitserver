// Interface lxc to simpler node function.

"use strict";
const EventEmitter = require("events");
var spawn = require("child_process").spawn;

//todo: assigner une branche a un conteneur et enregistrer, faire une page pour le webhook,faire les pulls sur les contenenurs, webserver,
class lxcLaunch extends EventEmitter {
    constructor(remote, container_name) {
        super();
        this.container_name = container_name;
        this.remote = remote;
        let lxc_launch_cmd = spawn("lxc", ["launch", remote, container_name, "-p", "default", "-p", "rep_access"]);
        lxc_launch_cmd.stdout.on("data", (data) => {
            this.emit("data",data.toString().replace(/\n\\r/,""));
        });
        lxc_launch_cmd.stderr.on("data", (data) => {
            this.emit("error", data);
        });
        lxc_launch_cmd.on("exit", (code) => {
            if(code !== 0) {
                throw "Exit code: "+code+" for lxc launch";
            }
            this.execBatch();
        });
    }
    execBatch(){
        let bash_exec = spawn("lxc", ["exec", this.container_name, "bash", "/repository/container_setup.sh"]);
        bash_exec.stdout.on("data", (data) => {
            this.emit("data",data.toString());
        });
        bash_exec.stderr.on("data", (data) => {
            let ignored_errror = [
                "W: The repository 'file:/repository ./ Release' does not have a Release file.\n",
                "debconf: delaying package configuration, since apt-utils is not installed\n",
                "\rExtracting templates from packages: 90%",
                "\rExtracting templates from packages: 100%\n"
            ];
            data = data.toString();
            if(ignored_errror.indexOf(data) === -1 && ignored_errror[0].indexOf(data) === -1) { //indexOf -1 mean not found, if error not found in ignored error, emit it. second part is because apt flush this erorr string in two random part.
                this.emit("error", data);
            }

        });
        bash_exec.on("exit", (code) => {
            if(code !== 0) {
                throw "Exit code: "+code+" for lxc launch";
            }
            this.emit("exit", code);
        });
    }
}
exports.launch = lxcLaunch;
function lxcList(callback){
    var buffer = "",
        lxc_list_cmd = spawn("lxc", ["list"]);
    lxc_list_cmd.stdout.on("data", function (data) {
        buffer += data.toString().replace(/\-|\+|\ /g,""); //regex: any + or -
    });

    lxc_list_cmd.stderr.on("data", function (data) {
        console.log("Shell command 'lxc list' error: " + data.toString());
    });
    lxc_list_cmd.on("exit", function (code) {
        if(code !== 0) {
            throw "Exit code: "+code+" for lxc list";
        }
        buffer = buffer.split("\n").filter(Boolean);
        buffer.shift();
        buffer.forEach(function(vm_string, index) {
            vm_string = vm_string.split("|");
            buffer[index] = {       // dont know if its better to make a loop or keep this,
                name: vm_string[1], // someone told me its better in case the lxc api change,
                state: vm_string[2],// but i think it will have the same finallity.
                IPv4: vm_string[3], // If someone see this i would like to have a second opinion
                IPv6: vm_string[4], // to validate what is the best practice.
                type: vm_string[5],
                snapshots: vm_string[6]
            };
        });
        callback(buffer);
    });
}
exports.list = lxcList;

function lxcStart(container_name, callback) {
    var lxc_cmd = spawn("lxc", ["start", container_name]);
    lxc_cmd.stderr.on("data", function (data) {
        console.log("Shell command 'lxc start' error: " + data.toString());
    });
    lxc_cmd.on("exit", function (code) {
        if(code !== 0) {
            throw "Exit code: "+code+" for lxc start";
        }
        callback();
    });
}

exports.start = lxcStart;

function lxcStop(container_name, callback) {
    var lxc_cmd = spawn("lxc", ["stop", container_name]);
    lxc_cmd.stderr.on("data", function (data) {
        console.log("Shell command 'lxc stop' error: " + data.toString());
    });
    lxc_cmd.on("exit", function (code) {
        if(code !== 0) {
            throw "Exit code: "+code+" for lxc list";
        }
        callback(code);
    });
}

exports.stop = lxcStop;

function lxcDelete(container_name, callback) {
    var lxc_cmd = spawn("lxc", ["delete", container_name]);
    lxc_cmd.stderr.on("data", function (data) {
        console.log("Shell command 'lxc delete' error: " + data.toString());
    });
    lxc_cmd.on("exit", function (code) {
        if(code !== 0) {
            throw "Exit code: "+code+" for lxc list";
        }
        callback();
    });
}

exports.delete = lxcDelete;
