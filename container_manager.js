//function managing the container using the lxc_manager
"use strict";
var lxc = require("./lxc_manager.js"),
    fs = require("fs");

Container.prototype.stop = function() {
    if (this.state === "running") {
        lxc.stop(this.container_name, (code)=>{
            if(code===0) {
                this.state = "stopped";
            }
        });
    } else {
        throw "Container "+this.container_name+" is not running, so he can't be stopped";
    }
};

Container.prototype.start = function() {
    if (this.state === "stopped") {
        lxc.start(this.container_name, (code)=>{
            if(code===0) {
                this.state = "running";
            }
        });
    } else {
        throw "Container "+this.container_name+" is not stopped, so he can't be started";
    }
};
Container.prototype.delete = function() {
    if (this.state === "stopped") {
        lxc.delete(this.container_name, (code)=>{
            if(code===0) {
                this.state = "deleted";
            }
        });
    } else {
        throw "Container "+this.container_name+" is not stopped, so he can't be deleted";
    }
};

Container.prototype.launch = function(callback) {
    if (this.state === "not_exist") {
        let container = new lxc.launch("images:debian/stretch", this.container_name);
        container.on("exit", (code) => {
            if(code !== 0 ) throw "exit code"+code;
            this.state = "started";
            this.install = "completed";
            callback();
        });
        container.on("data", (data) => {
            this.install = data;
            console.log(data);
        });
    } else {
        throw "Container "+this.container_name+" is not stopped, so he can't be deleted";
    }
};



Container.prototype.revive = function(object) {
    return Container(object.container_name, object.repository_branch, object.discord_api_key, object.creator_name);
};

function Container(container_name, repository_branch, discord_api_key, creator_name) {
    this.container_name = container_name;
    this.repository_branch = repository_branch;
    this.discord_api_key = discord_api_key;
    this.creator_name = creator_name;
    this.state = "not_exist";
    this.install = "not running";

}

ContainerArray.prototype =  Array.prototype;
ContainerArray.prototype.save = function(callback) {
    fs.writeFile("containers.info", JSON.stringify(this), "utf8", (err)=>{
        if (err) throw err;
        console.log("ok");
        callback();
    });
};
function ContainerArray(callback) {
    fs.readFile("containers.info", "utf8", (err, data)=> {
        if(err) throw err;
        data = JSON.parse(data);
        data.forEach((container_stored, index) => {
            this[index] = Container.prototype.revive(container_stored);
        });
        callback();
    });
}
//testing code to remove
var test = new Container("test12", "testbranch", "testapikey", "kuinox");
test.launch(function() {
    console.log("container launched");
    var test_container_array = new ContainerArray(function() {
        console.log("containerarray created");
        test_container_array.push(test);
        test_container_array.save(function() {
            console.log("saved");
        });
    });
});
